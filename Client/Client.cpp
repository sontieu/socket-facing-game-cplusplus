// Client.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Client.h"
#include "afxsock.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// The one and only application object

CWinApp theApp;

using namespace std;


bool checkName(char* name) {
	for (int i = 0; i < strlen(name); i++) {
		if (name[i] < '0' || (name[i] > '9' && name[i] < 'A') || (name[i] > 'Z' && name[i] < 'a') || name[i] > 'z')
			return false;
	}

	return true;
}

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	HMODULE hModule = ::GetModuleHandle(NULL);

	if (hModule != NULL)
	{
		// initialize MFC and print and error on failure
		if (!AfxWinInit(hModule, NULL, ::GetCommandLine(), 0))
		{
			// TODO: change error code to suit your needs
			_tprintf(_T("Fatal Error: MFC initialization failed\n"));
			nRetCode = 1;
		}
		else
		{
			// TODO: code your application's behavior here.
			const int PORT = 5467;
			const int MAX_NAME = 30;
			const int MAX_ARR = 255;

			CSocket client;
			char s_str[MAX_ARR], r_str[MAX_ARR];

			AfxSocketInit();
			client.Create();

			char name[MAX_NAME];
			char response[MAX_NAME];

			if (client.Connect(_T("127.0.0.1"), PORT)) {
				cout << "Ket noi thanh cong !" << endl;
				do {

					do {
						cout << "Nhap ten cua ban" << endl;
						cin.getline(name, MAX_NAME);

					} while (!checkName(name));

					int length = strlen(name);
					name[length] = NULL;

					client.Send(name, strlen(name) + 1);
					client.Receive(response, MAX_NAME);
				} while (strcmp(response, "succeed"));

				cout << "Dang ky voi server thanh cong." << endl;
				
				client.Receive(r_str, MAX_ARR);
				r_str[strlen(r_str)] = NULL;
				cout << "So nguoi choi : " << r_str << endl;
				
				client.Receive(r_str, MAX_ARR);
				r_str[strlen(r_str)] = NULL;
				cout << "So thu tu cua ban : " << r_str << endl;

				client.Receive(r_str, MAX_ARR);
				r_str[strlen(r_str)] = NULL;
				cout << "Tong so cau hoi trong tro choi nay : " << r_str << endl;

				cout << "Go \"skip\" de nhuong quyen (Luu y chi su dung duoc 1 lan)" << endl;

				// Bat dau choi game
				do {
					// Nhan cau hoi
					client.Receive(r_str, MAX_ARR);

					if (strcmp(r_str, "win") == 0) {
						cout << "Ban la nguoi chien thang" << endl;
						break;
					}
					else if (strcmp(r_str, "exit") == 0) {
						cout << "Da tim duoc nguoi chien thang" << endl;
						break;
					}

					cout << "Cau hoi : " << r_str << endl;
					// Tra loi
					cin.getline(s_str, MAX_ARR);
					client.Send(s_str, strlen(s_str) + 1);
					client.Receive(r_str, MAX_ARR);


					if (strcmp(r_str, "noskip") == 0) {
						cout << "Ban da dung het quyen nhuong" << endl;
					}
				} while (strcmp(r_str, "wrong"));
			}


			client.Close();
			cout << "Dong ket noi" << endl;
			cin.get();
		}
	}
	else
	{
		// TODO: change error code to suit your needs
		_tprintf(_T("Fatal Error: GetModuleHandle failed\n"));
		nRetCode = 1;
	}

	return nRetCode;
}


