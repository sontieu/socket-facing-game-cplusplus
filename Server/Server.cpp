// Server.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Server.h"
#include "afxsock.h"

#include <vector>
#include <fstream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <sstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif



const int PORT = 5467;
const int MAX_NAME = 30;
const int MAX_ARR = 255;


// The one and only application object

CWinApp theApp;

using namespace std;

bool isDuplicate(char* name, vector<char*> arr) {
	for (int i = 0; i < arr.size(); i++) {
		if (!strcmp(name, arr[i]))
			return false;
	}

	return true;
}

void getQA(vector<char*> &questions, vector<char*> &answers) {
	ifstream file;
	file.open("questions.txt");

	string str;
	while (getline(file, str, '\n')) {
		char q[MAX_ARR], a[MAX_ARR];

		istringstream iss(str);
		iss.getline(q, MAX_ARR, ';');
		iss.getline(a, MAX_ARR, '\n');

		questions.push_back(strdup(q));
		answers.push_back(strdup(a));
	}
}

int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	int nRetCode = 0;

	HMODULE hModule = ::GetModuleHandle(NULL);

	if (hModule != NULL)
	{
		// initialize MFC and print and error on failure
		if (!AfxWinInit(hModule, NULL, ::GetCommandLine(), 0))
		{
			// TODO: change error code to suit your needs
			_tprintf(_T("Fatal Error: MFC initialization failed\n"));
			nRetCode = 1;
		}
		else
		{
			// TODO: code your application's behavior here.
			CSocket server;
			int num_player = 2;
			int num_ques = 6;

			vector<CSocket*> client;
			
			for (int i = 0; i < num_player; i++)
				client.push_back(new CSocket());

			vector<char*> name_client;
			char s_str[MAX_ARR], r_str[MAX_ARR];

			vector<char*> questions;
			vector<char*> answers;
			getQA(questions, answers);

			vector<bool> skips;
			
			for (int i = 0; i < num_player; i++)
				skips.push_back(true);


			AfxSocketInit();
			srand(time(NULL));
			server.Create(PORT);
			server.Listen(num_player);


			cout << "Cho client ket noi...\n";


			// Set Name and Notice Info game 
			for (int i = 0; i < num_player; i++) {
				server.Accept(*client[i]);
				char name[MAX_NAME];
				bool setName = false;

				do {
					client[i]->Receive(name, MAX_NAME, 0);

					if (name_client.empty()
						|| isDuplicate(name, name_client)) {
						// available name
						name_client.push_back(strdup(name));
						client[i]->Send("succeed", strlen("succeed") + 1);
						setName = true;
					}
					else {
						// duplicate name
						client[i]->Send("duplicated", strlen("duplicated") + 1);
					}

				} while (setName == false);

				cout << name << " vua ket noi" << endl;

				itoa(num_player, s_str, 10);
				client[i]->Send(s_str, strlen(s_str) + 1);

				cout << "Gui so thu tu cho " << i << endl;

				itoa(i + 1, s_str, 10);
				client[i]->Send(s_str, strlen(s_str) + 1);

				cout << "Gui so cau hoi cho " << i << endl;

				itoa(num_ques, s_str, 10);
				client[i]->Send(s_str, strlen(s_str) + 1);
			}
			
			int now_player = 0;
			int iCauhoi;
			bool skip = false;

			do {
					CSocket* now_client = client[now_player];

					// Neu cau hoi da het
					if (questions.size() == 0) {
						now_client->Send("win", strlen("win") + 1);
						client.erase(client.begin() + now_player);
						now_client->Close();

						for (int i = 0; i < client.size(); i++) {
							client[i]->Send("exit", strlen("exit") + 1);
							client[i]->Close();
						}

						break;
					}

					// Chon random 1 cau hoi va 1 cau tra loi
					if (!skip)
						iCauhoi = rand() % questions.size();
					else
						skip = false;

					now_client->Send(questions[iCauhoi], strlen(questions[iCauhoi]) + 1);
					now_client->Receive(r_str, MAX_ARR);

					if (strcmp("skip", r_str) == 0)	// Nguoi choi su dung skip
					{
						bool allowSkip = skips[now_player];

						if (allowSkip) {
							skips[now_player] = false;

							if (now_player == client.size() - 1) { // the next player is the last one in turn
								now_player = 0;
							}
							else {
								now_player = now_player + 1;
							}

							now_client->Send("skipped", strlen("skipped") + 1);
						}
						else {	// no right to skip
							cout << "Thong bao den nguoi choi khong con quyen nhuong luot" << endl;
							now_client->Send("noskip", strlen("noskip") + 1);
							skip = true;
						}

					}
					else if (strcmp(answers[iCauhoi], r_str)) {
						// not right
						client.erase(client.begin() + now_player); // exit game
						
						if (client.size() == 1) {
							now_client->Send("wrong", strlen("wrong") + 1);	// notice to client
							now_client->Close(); // close connection

							cout << "Thong bao nguoi chien thang " << endl;

							CSocket* winner = client[0];
							winner->Send("win", strlen("win") + 1);
							winner->Close();

							break;
						}

						skips.erase(skips.begin() + now_player);

						now_client->Send("wrong", strlen("wrong") + 1);	// notice to client
						now_client->Close(); // close connection
					}
					else {
						// right answer
						if (now_player == client.size() - 1) { // the next player is the last one in turn
							now_player = 0;
						}
						else {
							now_player = now_player + 1;
						}
						now_client->Send("right", strlen("right") + 1);
					}


					if (!skip) {
						// Xoa bo cau hoi 
						questions.erase(questions.begin() + iCauhoi);
						answers.erase(answers.begin() + iCauhoi);
					}
			} while (client.size() != 0);

			for (int i = 0; i < client.size(); i++)
				client[i]->Close();
			server.Close();


			cout << "Server closed" << endl;
			cin.get();
		}
	}
	else
	{
		// TODO: change error code to suit your needs
		_tprintf(_T("Fatal Error: GetModuleHandle failed\n"));
		nRetCode = 1;
	}

	return nRetCode;
}
